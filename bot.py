import websocket, json, pprint, talib, talib, numpy
from binance.client import Client
from binance.enums import *
import config

SOCKET = "wss://stream.binance.com:9443/ws/ethusdt@kline_1m"

RSI_PERIOD = 14
RSI_OVERBOUGHT = 63 
RSI_OVERSOLD = 32
TRADE_SYMBOL = 'ETHUSD'
TRADE_QUANTITY = 0.025

closes = []
in_position = True 

client = Client(config.API_KEY, config.API_SECRET, tld='us')

def order(side, quantity, symbol, order_type):
    try:
        print("sending order")
        order = client.create_order(side=side, quantity=quantity, symbol=symbol, type=type)
    except Exception as e:
        return False
    return True

def on_open(ws):
    print('open connection')

def on_close(ws):
    print('close connection')

def on_message(ws, message):
    json_message = json.loads(message)
    #pprint.pprint(json_message)

    candle = json_message['k']

    is_candle_closed = candle['x']
    close = candle['c']
    
    if is_candle_closed:
        print("candle closed at {}".format(close))
        closes.append(float(close))
        print("closes")
        print(closes)

        if len(closes) > RSI_PERIOD:
            np_closes = numpy.array(closes)
            rsi = talib.RSI(np_closes, RSI_PERIOD)
            print("all rsi's calculated so far")
            print(rsi)
            last_rsi = rsi[-1]
            print("the current rsi is {}".format(last_rsi))

            if last_rsi > RSI_OVERBOUGHT:
                if in_position:
                    print("sell!")
                    order_succeeded = order(SIDE_SELL, TRADE_QUANTITY, TRADE_SYMBOL)
                    if order_succeeded:
                        in_position = False
                else:
                    print("missed opportunity, should have had some to sell")

            if last_rsi < RSI_OVERSOLD:
                if in_position:
                    print("oversold, but you already own it")
                else: 
                    print("Buy!")
                    order_succeeded = order(SIDE_BUY, TRADE_QUANTITY, TRADE_SYMBOL)
                    if order_succeeded:
                        in_position = True

ws = websocket.WebSocketApp(SOCKET, on_open=on_open, on_close=on_close, on_message=on_message)
ws.run_forever()